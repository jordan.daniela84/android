                Kontaktnachverfolgung
                
                Deine Daten, die für die Kontaktnachverfolgung erhoben und verarbeitet wurden, sind lokal in deiner App und verschlüsselt auf dem luca-Server gespeichert. Der luca-Server wird von der Deutschen Telekom AG gehostet. Zertifizierung zu dieser finden Sie unter: https://open-telekom-cloud.com/de/sicherheit/datenschutz-compliance. Ebenfalls stellt die Bundesdruckerei Gruppe GmbH weitere IT-Infrastrukturleistungen, die eine sichere Übermittlung deiner Daten an die Gesundheitsämter ermöglichen. Unser Unterauftragnehmer neXenio GmbH übernimmt die Softwareentwicklung, Softwarewartungs- und Softwarebetriebsleistungen. Sowohl wir als auch die nexenio GmbH können zu keinem Zeitpunkt deine Daten entschlüsseln und in der Klarform einsehen. Bei der Registrierung verifizieren wir deine Telefonnummer durch automatisierten SMS-Versand. Hierfür erfolgt die Übermittlung der Telefonnummer an unsere SMS-Versanddienstleister:innen. Die aktuellen Empfänger kannst du unserer aktuellen Datenschutzerklärung entnehmen: https://www.luca-app.de/app-privacy-policy/

Nur du kannst deine Daten innerhalb deiner App einsehen, bearbeiten und löschen. Wenn du bereits bei einer luca Location eingecheckt und ein Gesundheitsamt diese bei der Betreiber:in angefragt hat, werden die zweifach verschlüsselten Daten von der Betreiber:in und anschließend von dem Gesundheitsamt entschlüsselt. Nur das Gesundheitsamt kann auf die entschlüsselten Daten im Zuge der Kontaktnachverfolgung zugreifen und deine Daten einsehen. Deine Daten geben wir mit Ausnahme der Gesundheitsämter (Rechtsgrundlage Art. 6 Abs. 1 lit. c DSGVO in Verbindung mit der jeweiligen Landesverordnung zur Bekämpfung von COVID-19 Infektionen, Infektionsschutzgesetz) grundsätzlich nicht an Dritte weiter.
                
                Kontaktdaten

Name: Daniela Jordan
Adresse: schünbusch 12, 30855 langenhagen
Telefonnummer: 01773050841
E-Mail Adresse: jordan.daniela84@gmail.com
Speicherort: Lokales Gerät und luca Server
Speicherdauer: Löschung erfolgt 28 Tage nach der Löschung des Accounts oder jährlich am Ende jedes Jahres

Aufenthaltsdaten

Keine Einträge

                
                Alle Informationen über die Verarbeitungen, Zwecke, Rechtsgrundlagen und Löschfristen findest du in unserer Datenschutzerklärung: https://www.luca–app.de/app–privacy-policy/. Deine Daten werden nicht in Drittländer übermittelt. Automatisierte Entscheidungsfindungen sowie Profiling gemäß Art. 22 DSGVO werden von uns sowie den Auftragnehmern des luca Systems nicht getätigt.

Möchtest du eine Auskunft über deine Daten erhalten, die wir durch deine eigene Kontaktaufnahme (z. B. bei Supportanfragen) bei uns als Klardaten von dir erhalten haben, wende dich bitte an unser Privacy Team unter privacy@culture4life.de. Bitte teile uns auch weitere Hinweise mit, die uns ermöglichen, dich zu identifizieren und dazugehörige Daten bei uns zu recherchieren. Wenn du mit uns über Social Media in Kontakt warst, sind weitere Hinweise wie deine E-Mail-Adresse oder dein Pseudonym, das du in den Social-Media-Kanälen verwendest, wichtig um dir die Auskunft erteilen zu können.

Du hast gemäß Art. 77 DSGVO das Recht, dich bei der Aufsichtsbehörde zu beschweren, wenn du der Ansicht bist, dass die Verarbeitung deiner personenbezogenen Daten nicht rechtmäßig erfolgt. Die Anschrift der für unser Unternehmen zuständigen Aufsichtsbehörde lautet: Berliner Beauftragte für Datenschutz und Informationsfreiheit, Friedrichstr. 219, 10969 Berlin, Tel.: 030/13889-0, E-Mail: mailbox@datenschutz-berlin.de.